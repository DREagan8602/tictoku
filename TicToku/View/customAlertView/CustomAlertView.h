//
//  CustomAlertView.h
//  TicTac
//
//  Created by Raza Najam on 3/12/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *winningImgView;
@property (weak, nonatomic) IBOutlet UILabel *winnerStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *winnerNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextStageButton;
@property (weak, nonatomic) IBOutlet UIButton *playAgainButton;
@property (weak, nonatomic) IBOutlet UIButton *quitButton;


@end
