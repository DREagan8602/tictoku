//
//  CustomAlertView.m
//  TicTac
//
//  Created by Raza Najam on 3/12/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "CustomAlertView.h"

@implementation CustomAlertView

-(void)awakeFromNib {
    _winnerNameLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:20];
}
@end
