//
//  ViewController.h
//  TicTac
//
//  Created by Raza Najam on 2/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCTurnBasedMatchHelper.h"
@interface MenuViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *optionView;
- (IBAction)doubleGamePlayerButtonClicked:(id)sender;
- (IBAction)gameCenterButtonClicked:(id)sender;

@end

