//
//  ViewController.m
//  TicTac
//
//  Created by Raza Najam on 2/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "MenuViewController.h"
#import "TwoPlayerViewController.h"
#import "DoubleViewController.h"
#import "InstructionViewController.h"
#define IS_IPHONE_4 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )480 ) < DBL_EPSILON )

@interface MenuViewController ()

@end

@implementation MenuViewController

-(void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = YES;
    NSString *string = [[NSUserDefaults standardUserDefaults]objectForKey:@"First Run"];
    if (string == nil) {
        InstructionViewController *instruction = [self.storyboard instantiateViewControllerWithIdentifier:@"InstructionViewController"];
        [self.navigationController pushViewController:instruction animated:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loadLoginVieGameCenter:) name:@"loadTwoPlayerScreen" object:nil];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadLoginVieGameCenter:(NSNotification *)notif {
//    NSDictionary *dictt = (NSDictionary *)notif.userInfo;
//    UIViewController *gameController = [dictt objectForKey:@"controller"];
//    [self presentViewController:gameController animated:YES completion:nil];
    
    TwoPlayerViewController *twoPlayer = (TwoPlayerViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"TwoPlayerViewController"];
    [self.navigationController pushViewController:twoPlayer animated:YES];
}

- (IBAction)doubleGamePlayerButtonClicked:(id)sender {
    UIViewController *doubleViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DoubleViewController"];
    [self.navigationController pushViewController:doubleViewController animated:YES];
}

- (IBAction)gameCenterButtonClicked:(id)sender {
    [[GCTurnBasedMatchHelper sharedInstance]
     findMatchWithMinPlayers:2 maxPlayers:2 viewController:self];
}


@end