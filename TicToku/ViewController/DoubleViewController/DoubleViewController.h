//
//  DoubleViewController.h
//  TicTac
//
//  Created by Raza Najam on 2/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoubleViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>{
    int playerSelected;
}

@property (retain, nonatomic) UIImagePickerController *imagePickerController;
@property (weak, nonatomic) IBOutlet UIImageView *playerOneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *playerTwoImageView;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextLabel;
@property (weak, nonatomic) IBOutlet UITextField *secondNameTextLabel;

- (IBAction)startButtonClicked:(id)sender;
- (IBAction)player1ImageButtonClicked:(id)sender;
- (IBAction)player2ImageButtonClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;
@end
