//
//  DoubleViewController.m
//  TicTac
//
//  Created by Raza Najam on 2/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "DoubleViewController.h"
#import "GameViewController.h"
@interface DoubleViewController ()

@end

@implementation DoubleViewController

#pragma mark - Methods -
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self managingUI];
    _playerOneImageView.transform = CGAffineTransformMakeRotation(-.0550);
    _playerTwoImageView.transform = CGAffineTransformMakeRotation(-.0550);
    _firstNameTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:12];
    _firstNameTextLabel.delegate = self;
    _firstNameTextLabel.returnKeyType = UIReturnKeyNext;
    _secondNameTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:12];
    _secondNameTextLabel.delegate = self;
    _secondNameTextLabel.returnKeyType = UIReturnKeyDone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)startButtonClicked:(id)sender {
    GameViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GameViewController"];
    viewController.playerOneImage = _playerOneImageView.image;
    viewController.playerTwoImage = _playerTwoImageView.image;
    viewController.playerOneName = _firstNameTextLabel.text;
    viewController.playerTwoName = _secondNameTextLabel.text;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)player1ImageButtonClicked:(id)sender {
    playerSelected = 1;
    _imagePickerController = [[UIImagePickerController alloc]init];
    _imagePickerController.delegate = self;
    _imagePickerController.allowsEditing = YES;
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (IBAction)player2ImageButtonClicked:(id)sender {
    playerSelected = 2;
    _imagePickerController = [[UIImagePickerController alloc]init];
    _imagePickerController.delegate = self;
    _imagePickerController.allowsEditing = YES;
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *img = info[UIImagePickerControllerEditedImage];
    if (playerSelected == 1) {
        _playerOneImageView.image = img;
    }else{
        _playerTwoImageView.image = img;
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}


-(void)managingUI       {
    if ([_firstNameTextLabel respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor lightGrayColor];
        _firstNameTextLabel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Player Name" attributes:@{NSForegroundColorAttributeName: color}];
        _secondNameTextLabel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Second Player Name" attributes:@{NSForegroundColorAttributeName: color}];

    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _firstNameTextLabel) {
        [_secondNameTextLabel becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return NO;
}

@end
