//
//  GameViewController.m
//  TicTac
//
//  Created by Raza Najam on 2/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "GameViewController.h"
#import "WinnerViewController.h"
#import "CustomAlertView.h"
#import "Shared.h"

typedef enum
{
    GW_FALSE,
    GW_TRUE,
    GW_DRAW
} GameWinner;

@implementation NSArray (Random)

- (id) randomObject
{
    if ([self count] == 0) {
        return nil;
    }
    return [self objectAtIndex: arc4random() % [self count]];
}

@end

@interface GameViewController ()

@end

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _playerOneNameLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:14];
    _playerTwoNameLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:14];
    _headerPlayerOneNameLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:14];
    _headerPlayerTwoNameLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:14];
    
    self.navigationController.navigationBarHidden = YES;
    playedValues = 0;
    currentTargetedGame = 1;
    if (_gamePlayer == 1) {
        [self singlePlayerGame];
    }else{
        [self doublePlayerGame];
    }
    checkWinner = true;
    _player1ValuesDict = [[NSMutableDictionary alloc]init];
    _player2ValuesDict = [[NSMutableDictionary alloc]init];
    _winningMutableDictionary = [[NSMutableDictionary alloc]init];
    playerTurn=1;
    playedBy = 0;
    _targetGameNumber = 7;    
    if ([[ UIScreen mainScreen ] bounds ].size.height>480)  {
        _numbView.frame = CGRectMake(_numbView.frame.origin.x, 400, _numbView.frame.size.width, _numbView.frame.size.height);
        _brdView.frame = CGRectMake(_brdView.frame.origin.x, 132, _brdView.frame.size.width, _brdView.frame.size.height);
        _topBrdView.frame = CGRectMake(_topBrdView.frame.origin.x, 85, _topBrdView.frame.size.width, _topBrdView.frame.size.height);
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [self resetGame];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)singlePlayerGame {
    _player2Number1Button.userInteractionEnabled = NO;
    _player2Number2Button.userInteractionEnabled = NO;
    _player2Number3Button.userInteractionEnabled = NO;
    _player2Number4Button.userInteractionEnabled = NO;
    _player2Number5Button.userInteractionEnabled = NO;

    _playerOneTurnImageView.hidden = NO;
    _playerTwoTurnImageView.hidden = YES;
    _player1ImageView.image = _playerOneImage;
    _player2ImageView.image = [UIImage imageNamed:@"computerPlayer.jpeg"];
    if (_playerOneImage == nil) {
        _player1ImageView.image = [UIImage imageNamed:@"avatarFirstPlayerImage.jpeg"];
    }
    if ([_playerOneName isEqualToString:@""]) {
        _playerOneName = @"Player 1";
    }
    _playerOneNameLabel.text = [NSString stringWithFormat:@"%@",_playerOneName];
    _playerTwoNameLabel.text = @"Professor";
    _headerPlayerOneNameLabel.text = _playerOneName;
    _headerPlayerTwoNameLabel.text = @"Professor";
    _computerNumbers = [[NSMutableArray alloc]init];
    _computerPlacements = [[NSMutableArray alloc]init];
    [self fillComputerPlayerValues];
}

-(void)doublePlayerGame {
    _playerOneTurnImageView.hidden = NO;
    _playerTwoTurnImageView.hidden = YES;
    
    _player1ImageView.image = _playerOneImage;
    _player2ImageView.image = _playerTwoImage;
    if (_playerOneImage == nil) {
        _player1ImageView.image = [UIImage imageNamed:@"avatarFirstPlayerImage.jpeg"];
    }
    if (_playerTwoImage == nil) {
        _player2ImageView.image = [UIImage imageNamed:@"computerPlayer.jpeg"];
    }
    
    if ([_playerOneName isEqualToString:@""] || _playerOneName == nil) {
        _playerOneName = @"Player 1";
    }
    if ([_playerTwoName isEqualToString:@""]|| _playerTwoName == nil) {
        _playerTwoName = @"Player 2";
    }
    _playerOneNameLabel.text = _playerOneName;
    _playerTwoNameLabel.text = _playerTwoName;
    _headerPlayerOneNameLabel.text = _playerOneName;
    _headerPlayerTwoNameLabel.text = _playerTwoName;
}

-(void)fillComputerPlayerValues {
    [_computerNumbers addObject:[NSNumber numberWithInt:1]];
    [_computerNumbers addObject:[NSNumber numberWithInt:2]];
    [_computerNumbers addObject:[NSNumber numberWithInt:3]];
    [_computerNumbers addObject:[NSNumber numberWithInt:4]];
    [_computerNumbers addObject:[NSNumber numberWithInt:5]];
}

- (IBAction)boardButtonClicked:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (playedBy == 1 && playerTurn == 1) {
        _playerTwoTurnImageView.hidden = NO;
        _playerOneTurnImageView.hidden = YES;
        playedValues++;
        NSString *imgName = [NSString stringWithFormat:@"boardP1Number%ld", (long)player1SelectedValue];
        [btn setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
        [_player1ValuesDict setObject:[NSNumber numberWithInt:player1SelectedValue] forKey:[NSString stringWithFormat:@"%ld",(long)[sender tag]]];
        playerTurn = 2;
        GameWinner winningPlayerCheck = [self checkWinnerPlayerOne];
        btn.userInteractionEnabled = NO;
        _selectedBtn.hidden = YES;
        [Shared.instance startMusic:@"turnPlayed"];
        //For single player
        if (_gamePlayer == 1) {
            switch (winningPlayerCheck) {
                case GW_FALSE:
                    [self performSelector:@selector(computerTurnToPlayGame) withObject:nil afterDelay:3];
                    break;
                    
                case GW_TRUE:
                    [self performSelector:@selector(displayingAlert) withObject:nil afterDelay:2];
                    break;
                
                case GW_DRAW:
                    [self performSelector:@selector(showCustomAlert) withObject:nil afterDelay:2];
                    break;
            }
        } else {
            switch (winningPlayerCheck) {
                case GW_TRUE:
                    [self performSelector:@selector(displayingAlert) withObject:nil afterDelay:2];
                    break;
                    
                case GW_DRAW:
                    [self performSelector:@selector(showCustomAlert) withObject:nil afterDelay:2];
                    break;
                    
                default:
                    break;
            }
        }
        return;
    }
    
    if (playedBy == 2 && playerTurn == 2) {
        [Shared.instance startMusic:@"opponentTurn"];
        _playerOneTurnImageView.hidden = NO;
        _playerTwoTurnImageView.hidden = YES;
        playedValues++;
        NSString *imgName = [NSString stringWithFormat:@"boardP2Number%ld", (long)player2SelectedValue];
        [btn setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
        [_player2ValuesDict setObject:[NSNumber numberWithInt:player2SelectedValue] forKey:[NSString stringWithFormat:@"%ld",(long)[sender tag]]];
        btn.userInteractionEnabled = NO;
        _selectedBtn.hidden = YES;
        playerTurn = 1;
        GameWinner winningPlayerCheck = [self checkWinnerPlayerOne];
        switch (winningPlayerCheck) {
            case GW_TRUE:
                [self performSelector:@selector(displayingAlert) withObject:nil afterDelay:2];
                break;
                
            case GW_DRAW:
                [self performSelector:@selector(showCustomAlert) withObject:nil afterDelay:2];
                break;
                
            default:
                break;
        }
    }
}

- (IBAction)player1NumbersClicked:(id)sender {
    if (checkWinner) {
        if (playerTurn == 1) {
            _selectedBtn = (UIButton *)sender;
            player1SelectedValue = [sender tag];
            playedBy = 1;
            [self selectionofPlayerOneButton:[sender tag]];
        }
    }
}

- (IBAction)player2NumbersClicked:(id)sender {
    if (checkWinner) {
        if (playerTurn == 2) {
            _selectedBtn = (UIButton *)sender;
            player2SelectedValue = [sender tag];
            playedBy = 2;
            [self selectionofPlayerTwoButton:[sender tag]];
        }
    }
}

//Computer's turn to play game
-(void)computerTurnToPlayGame  {
    _playerOneTurnImageView.hidden = NO;
    _playerTwoTurnImageView.hidden = YES;
    if (playedValues == 9 ) {
        return;
    }
    
    [_computerPlacements removeAllObjects];
    [Shared.instance startMusic:@"opponentTurn"];

    BOOL check;
    NSArray *playerOnePlayedValues = [_player1ValuesDict allKeys];
    NSArray *playerTwoPlayedValues = [_player2ValuesDict allKeys];
    NSArray *allPlayedValArray = [playerOnePlayedValues arrayByAddingObjectsFromArray:playerTwoPlayedValues];
    
    if (checkWinner == true) {
        for (int i = 1; i<10; i++) {
            check = true;
            for(NSString *Val in allPlayedValArray){
                if ([Val intValue] == i) {
                    check=false;
                }
            }
            if (check) {
                [_computerPlacements addObject:[NSNumber numberWithInt:i]];
            }
        }
        
        NSNumber *num = [_computerNumbers randomObject];
        NSNumber *placementNum = [_computerPlacements randomObject];
        [_player2ValuesDict setObject:num forKey:[NSString stringWithFormat:@"%@", placementNum]];
        
        
        for (int i=0; i < [_computerNumbers count]; i++) {
            NSString *item = [_computerNumbers objectAtIndex:i];
            if ([num intValue] == [item intValue]) {
                [_computerNumbers removeObject:item];
                i--;
            }
        }
        
        switch ([num intValue]) {
            case 1:
                _player2Number1Button.hidden = YES;
                break;
            case 2:
                _player2Number2Button.hidden = YES;
                break;
            case 3:
                _player2Number3Button.hidden = YES;
                break;
            case 4:
                _player2Number4Button.hidden = YES;
                break;
            case 5:
                _player2Number5Button.hidden = YES;
                break;
            default:
                break;
        }
        
        switch ([placementNum intValue]) {
            case 1:{
                NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@", num];
                [_btn1 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                _btn1.userInteractionEnabled = NO;
            }
                break;
            case 2:{
                NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@", num];
                [_btn2 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                _btn2.userInteractionEnabled = NO;
            }
                break;
            case 3:{
                NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@", num];
                [_btn3 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                _btn3.userInteractionEnabled = NO;
            }
                break;
            case 4:{
                NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@", num];
                [_btn4 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                _btn4.userInteractionEnabled = NO;
            }
                break;
            case 5:{
                NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@", num];
                [_btn5 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                _btn5.userInteractionEnabled = NO;
            }
                break;
            case 6:{
                NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@", num];
                [_btn6 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                _btn6.userInteractionEnabled = NO;
            }
                break;
            case 7:{
                NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@", num];
                [_btn7 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                _btn7.userInteractionEnabled = NO;
            }
                break;
            case 8:{
                NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@", num];
                [_btn8 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                _btn8.userInteractionEnabled = NO;
            }
                break;
            case 9:{
                NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@", num];
                [_btn9 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                _btn9.userInteractionEnabled = NO;
            }
                break;
            default:
                break;
        }
        playedValues++;
        playerTurn=1;
        BOOL winner = [self checkWinnerPlayerOne];
        if (winner) {
            [self performSelector:@selector(displayingAlert) withObject:nil afterDelay:2];
        }
        playedBy=2;
    }
}

//Checking winner...
-(GameWinner)checkWinnerPlayerOne
{
    BOOL winner =false;
    int row1col1Player2 =  [[_player2ValuesDict objectForKey:@"1"] intValue];
    int row1col2Player2 =  [[_player2ValuesDict objectForKey:@"2"] intValue];
    int row1col3Player2 =  [[_player2ValuesDict objectForKey:@"3"] intValue];
    int row2col1Player2 =  [[_player2ValuesDict objectForKey:@"4"] intValue];
    int row2col2Player2 =  [[_player2ValuesDict objectForKey:@"5"] intValue];
    int row2col3Player2 =  [[_player2ValuesDict objectForKey:@"6"] intValue];
    int row3col1Player2 =  [[_player2ValuesDict objectForKey:@"7"] intValue];
    int row3col2Player2 =  [[_player2ValuesDict objectForKey:@"8"] intValue];
    int row3col3Player2 =  [[_player2ValuesDict objectForKey:@"9"] intValue];
    
    int row1col1 =  [[_player1ValuesDict objectForKey:@"1"] intValue];
    int row1col2 =  [[_player1ValuesDict objectForKey:@"2"] intValue];
    int row1col3 =  [[_player1ValuesDict objectForKey:@"3"] intValue];
    int row2col1 =  [[_player1ValuesDict objectForKey:@"4"] intValue];
    int row2col2 =  [[_player1ValuesDict objectForKey:@"5"] intValue];
    int row2col3 =  [[_player1ValuesDict objectForKey:@"6"] intValue];
    int row3col1 =  [[_player1ValuesDict objectForKey:@"7"] intValue];
    int row3col2 =  [[_player1ValuesDict objectForKey:@"8"] intValue];
    int row3col3 =  [[_player1ValuesDict objectForKey:@"9"] intValue];
    int calculateValue;
    
    if ((row1col1 != 0||row1col1Player2!= 0) && (row1col2!= 0|| row1col2Player2!= 0) && (row1col3 != 0 || row1col3Player2!= 0)) {
        calculateValue  = row1col1+row1col2+row1col3+row1col1Player2+row1col2Player2+row1col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:1 :1];
            winner = true;
        }
    }
    if((row2col1 != 0|| row2col1Player2!= 0) && (row2col2 != 0||row2col2Player2 ) && (row2col3 != 0||row2col3Player2 != 0 )) {
        calculateValue  = row2col1+row2col2+row2col3+row2col1Player2+row2col2Player2+row2col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:2 :1];
            winner = true;
        }
    }
    if((row3col1 != 0 || row3col1Player2 != 0) && (row3col2 !=0 || row3col2Player2 !=0 ) && (row3col3 != 0 || row3col3Player2 !=0 )) {
        calculateValue  = row3col1+row3col2+row3col3+row3col1Player2+row3col2Player2+row3col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:3 :1];
            winner = true;
        }
    }
    if((row1col1 != 0 || row1col1Player2 != 0) && (row2col1 != 0 || row2col1Player2 !=0) && (row3col1 != 0 ||row3col1Player2 !=0)) {
        calculateValue  = row1col1+row2col1+row3col1 + row1col1Player2+row2col1Player2+row3col1Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:4 :1];
            winner = true;
        }
    }
    if((row1col2 != 0 || row1col2Player2 != 0) && (row2col2 != 0 || row2col2Player2 != 0) && (row3col2 != 0 ||row3col2Player2 != 0)) {
        calculateValue  = row1col2+row2col2+row3col2+row1col2Player2+row2col2Player2+row3col2Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:5 :1];
            winner = true;
        }
    }
    if((row1col3 != 0 || row1col3Player2 != 0) && (row2col3 != 0 || row2col3Player2 != 0) && (row3col3 != 0 || row3col3Player2 != 0)) {
        calculateValue  = row1col3+row2col3+row3col3+row1col3Player2+row2col3Player2+row3col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:6 :1];
            winner = true;
        }
    }
    if((row1col1 != 0 || row1col1Player2 != 0) && (row2col2 != 0|| row2col2Player2 != 0) && (row3col3 != 0 || row3col3Player2 != 0)) {
        calculateValue  = row1col1+row2col2+row3col3+row1col1Player2+row2col2Player2+row3col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:7 :1];
            winner = true;
        }
    }
    if((row1col3 != 0 || row1col3Player2 != 0) && (row2col2 != 0 || row2col2Player2 != 0) && (row3col1 != 0 || row3col1Player2 != 0)) {
        calculateValue  = row1col3+row2col2+row3col1+row1col3Player2+row2col2Player2+row3col1Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:8 :1];
            winner = true;
        }
    }
    
    if (winner) {
        [self performSelectorOnMainThread:@selector(playSoundOnMainThread) withObject:nil waitUntilDone:NO];
        if (playerTurn == 1) {
            _whoWinns = _playerOneName;
            [_winningMutableDictionary setObject:@"2" forKey:[NSNumber numberWithInt:_targetGameNumber]];
        }else{
            _whoWinns = _playerTwoName;
            [_winningMutableDictionary setObject:@"1" forKey:[NSNumber numberWithInt:_targetGameNumber]];
        }
        NSLog(@"%@", _winningMutableDictionary);
        return GW_TRUE;
    }
    
    if (playedValues >= 9) {
        if(currentTargetedGame >= 5) {
            return GW_TRUE;
        }else{
            return GW_DRAW;
        }
        _whoWinns = @"";
        return GW_FALSE;
    }
    
    return GW_FALSE;
}

-(void)playSoundOnMainThread    {
    [Shared.instance startMusic:@"winningSound"];
}

-(void)showCustomAlert      {
    _customView = [[[NSBundle mainBundle]loadNibNamed:@"CustomAlertView" owner:self options:nil]objectAtIndex:0];
    _customView.center = self.view.center;
    [_customView.nextStageButton addTarget:self action:@selector(nextStageButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [_customView.playAgainButton addTarget:self action:@selector(playAgainButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [_customView.quitButton addTarget:self action:@selector(quitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    _customView.winnerNameLabel.hidden = YES;
    _customView.winnerStaticLabel.text = @"Draw";
    _customView.winningImgView.image = [UIImage imageNamed:@"DrawGameImage.png"];
    [self.view addSubview:_customView];
}

-(void)displayingAlert  {
    if (playerTurn == 2) {
        _customView = [[[NSBundle mainBundle]loadNibNamed:@"CustomAlertView" owner:self options:nil]objectAtIndex:0];
        _customView.center = self.view.center;
        [_customView.nextStageButton addTarget:self action:@selector(nextStageButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_customView.playAgainButton addTarget:self action:@selector(playAgainButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_customView.quitButton addTarget:self action:@selector(quitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        _customView.winningImgView.image = [UIImage imageNamed:@"winnerGameImage.png"];
        _customView.winnerNameLabel.text = _playerOneName;
        [self.view addSubview:_customView];
    }else{
        _customView = [[[NSBundle mainBundle]loadNibNamed:@"CustomAlertView" owner:self options:nil]objectAtIndex:0];
        _customView.center = self.view.center;
        [_customView.nextStageButton addTarget:self action:@selector(nextStageButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_customView.playAgainButton addTarget:self action:@selector(playAgainButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_customView.quitButton addTarget:self action:@selector(quitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        _customView.winnerNameLabel.text = _playerTwoName;
        _customView.winningImgView.image = [UIImage imageNamed:@"winnerGameImage.png"];
        [self.view addSubview:_customView];
    }
}

-(void)presentingLineOnGame:(int)position :(int)player     {
    switch (position) {
        case 1:
            [self fadeInImage:_horizontal1ImageView];
            break;
        case 2:
            [self fadeInImage:_horizontal2ImageView];
            break;
        case 3:
            [self fadeInImage:_horizontal3ImageView];
            break;
        case 4:
            [self fadeInImage:_vertical1ImageView];
            break;
        case 5:
            [self fadeInImage:_vertical2ImageView];
            break;
        case 6:
            [self fadeInImage:_vertical3ImageView];
            break;
        case 7:
            [self fadeInImage:_diagnalLeftImageView];
            break;
        case 8:
            [self fadeInImage:_diagnalRightImageView];
            break;
        default:
            break;
    }
}

-(void)showWinnerScreen {
    currentTargetedGame = 1;
    UIButton *btn = [[UIButton alloc]init];
    btn.tag = currentTargetedGame;
    [self targetButtonClicked:btn];
    WinnerViewController *winner = [self.storyboard instantiateViewControllerWithIdentifier:@"WinnerViewController"];
    winner.winnerDict = _winningMutableDictionary;
    winner.playerOneName = _playerOneName;
    winner.playerTwoName = _playerTwoName;
    [self.navigationController pushViewController:winner animated:NO];
}

-(void)fadeInImage:(UIImageView *)img{
    [UIView animateWithDuration:0.3
                          delay:0.3
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{ img.alpha = 1; }
                     completion:^(BOOL finished){}
     ];
    checkWinner = false;
}

-(void)showAler:(NSString *)messageString       {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Tic Toku" message:messageString delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
}

//Select Target game
- (IBAction)targetButtonClicked:(id)sender {
    switch ([sender tag]) {
        case 1:
            _targetNumber7Button.selected = true;
            _targetNumber8Button.selected = false;
            _targetNumber9Button.selected = false;
            _targetNumber10Button.selected = false;
            _targetNumber11Button.selected = false;
            _targetGameNumber = 7;
            break;
        case 2:
            _targetNumber7Button.selected = false;
            _targetNumber8Button.selected = true;
            _targetNumber9Button.selected = false;
            _targetNumber10Button.selected = false;
            _targetNumber11Button.selected = false;
            _targetGameNumber=8;
            break;
        case 3:
            _targetNumber7Button.selected = false;
            _targetNumber8Button.selected = false;
            _targetNumber9Button.selected = true;
            _targetNumber10Button.selected = false;
            _targetNumber11Button.selected = false;
            _targetGameNumber=9;
            break;
        case 4:
            _targetNumber7Button.selected = false;
            _targetNumber8Button.selected = false;
            _targetNumber9Button.selected = false;
            _targetNumber10Button.selected = true;
            _targetNumber11Button.selected = false;
            _targetGameNumber=10;
            break;
        case 5:
            _targetNumber7Button.selected = false;
            _targetNumber8Button.selected = false;
            _targetNumber9Button.selected = false;
            _targetNumber10Button.selected = false;
            _targetNumber11Button.selected = true;
            _targetGameNumber=11;
            break;
        default:
            break;
    }
    _targetNumberTextLabel.text = [NSString stringWithFormat:@"TARGET IS %d",_targetGameNumber];
}

//Selection of number by player 1...
-(void)selectionofPlayerOneButton:(int)selectedTag {
    
    switch (selectedTag) {
        case 1:
            _player1Number1Button.selected = true;
            _player1Number2Button.selected = false;
            _player1Number3Button.selected = false;
            _player1Number4Button.selected = false;
            _player1Number5Button.selected = false;
            break;
        case 2:
            _player1Number1Button.selected = false;
            _player1Number2Button.selected = true;
            _player1Number3Button.selected = false;
            _player1Number4Button.selected = false;
            _player1Number5Button.selected = false;
            break;
        case 3:
            _player1Number1Button.selected = false;
            _player1Number2Button.selected = false;
            _player1Number3Button.selected = true;
            _player1Number4Button.selected = false;
            _player1Number5Button.selected = false;
            break;
        case 4:
            _player1Number1Button.selected = false;
            _player1Number2Button.selected = false;
            _player1Number3Button.selected = false;
            _player1Number4Button.selected = true;
            _player1Number5Button.selected = false;
            break;
        case 5:
            _player1Number1Button.selected = false;
            _player1Number2Button.selected = false;
            _player1Number3Button.selected = false;
            _player1Number4Button.selected = false;
            _player1Number5Button.selected = true;
            break;
        default:
            break;
    }
}

//Selection of number by player 12...
-(void)selectionofPlayerTwoButton:(int)selectedTag {
    
    switch (selectedTag) {
        case 1:
            _player2Number1Button.selected = true;
            _player2Number2Button.selected = false;
            _player2Number3Button.selected = false;
            _player2Number4Button.selected = false;
            _player2Number5Button.selected = false;
            break;
        case 2:
            _player2Number1Button.selected = false;
            _player2Number2Button.selected = true;
            _player2Number3Button.selected = false;
            _player2Number4Button.selected = false;
            _player2Number5Button.selected = false;
            break;
        case 3:
            _player2Number1Button.selected = false;
            _player2Number2Button.selected = false;
            _player2Number3Button.selected = true;
            _player2Number4Button.selected = false;
            _player2Number5Button.selected = false;
            break;
        case 4:
            _player2Number1Button.selected = false;
            _player2Number2Button.selected = false;
            _player2Number3Button.selected = false;
            _player2Number4Button.selected = true;
            _player2Number5Button.selected = false;
            break;
        case 5:
            _player2Number1Button.selected = false;
            _player2Number2Button.selected = false;
            _player2Number3Button.selected = false;
            _player2Number4Button.selected = false;
            _player2Number5Button.selected = true;
            break;
        default:
            break;
    }
}

-(void)resetGame {
    
    _player1Number1Button.hidden = NO;
    _player1Number2Button.hidden = NO;
    _player1Number3Button.hidden = NO;
    _player1Number4Button.hidden = NO;
    _player1Number5Button.hidden = NO;
    _player2Number1Button.hidden = NO;
    _player2Number2Button.hidden = NO;
    _player2Number3Button.hidden = NO;
    _player2Number4Button.hidden = NO;
    _player2Number5Button.hidden = NO;
    [_btn1 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn2 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn3 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn4 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn5 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn6 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn7 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn8 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn9 setBackgroundImage:nil forState:UIControlStateNormal];
    _btn1.userInteractionEnabled = YES;
    _btn2.userInteractionEnabled = YES;
    _btn3.userInteractionEnabled = YES;
    _btn4.userInteractionEnabled = YES;
    _btn5.userInteractionEnabled = YES;
    _btn6.userInteractionEnabled = YES;
    _btn7.userInteractionEnabled = YES;
    _btn8.userInteractionEnabled = YES;
    _btn9.userInteractionEnabled = YES;
    
    [_player1ValuesDict removeAllObjects];
    [_player2ValuesDict removeAllObjects];
    //    playerTurn=1;
    _diagnalRightImageView.alpha = 0;
    _diagnalLeftImageView.alpha = 0;
    _horizontal1ImageView.alpha = 0;
    _horizontal2ImageView.alpha = 0;
    _horizontal3ImageView.alpha = 0;
    _vertical1ImageView.alpha = 0;
    _vertical2ImageView.alpha = 0;
    _vertical3ImageView.alpha = 0;
    checkWinner = true;
    playedBy= 0;
    playedValues = 0;
    _player2Number1Button.selected = false;
    _player2Number2Button.selected = false;
    _player2Number3Button.selected = false;
    _player2Number4Button.selected = false;
    _player2Number5Button.selected = false;
    _player1Number1Button.selected = false;
    _player1Number2Button.selected = false;
    _player1Number3Button.selected = false;
    _player1Number4Button.selected = false;
    _player1Number5Button.selected = false;
    [_computerNumbers removeAllObjects];
    [_computerPlacements removeAllObjects];
    [self fillComputerPlayerValues];
}

#pragma mark - customViewAlert button...
-(void)nextStageButtonClicked   {
    if (!(currentTargetedGame >= 5)) {
        currentTargetedGame++;
        UIButton *btn = [[UIButton alloc]init];
        btn.tag = currentTargetedGame;
        [self targetButtonClicked:btn];
        [self resetGame];
        [_customView removeFromSuperview];
        if (_gamePlayer == 1) {
            if (playerTurn == 2) {
                [self performSelector:@selector(computerTurnToPlayGame) withObject:nil afterDelay:2];
            }
        }
    } else {
        [self showWinnerScreen];
    }
}

-(void)playAgainButtonClicked   {
    [self resetGame];
    [_customView removeFromSuperview];
    if (_gamePlayer == 1) {
        if (playerTurn == 2) {
            [self performSelector:@selector(computerTurnToPlayGame) withObject:nil afterDelay:2];
        }
    }
}

-(void)quitButtonClicked   {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end