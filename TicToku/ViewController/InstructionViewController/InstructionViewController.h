//
//  InstructionViewController.h
//  TicTac
//
//  Created by Raza Najam on 2/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstructionViewController : UIViewController<UIScrollViewDelegate, UINavigationControllerDelegate>
@property (nonatomic, retain) NSArray *introuductionArray;
@property (nonatomic, retain) NSArray *descriptionArray;
@property (nonatomic, retain) NSArray *headingArray;
@property (weak, nonatomic) IBOutlet UICollectionView *introductionCollectionVIew;
@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIPageControl *introductionPageView;


- (IBAction)skipButtonClicked:(id)sender;
- (IBAction)startButtonClicked:(id)sender;
@end
