//
//  InstructionViewController.m
//  TicTac
//
//  Created by Raza Najam on 2/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "InstructionViewController.h"
#import "GCTurnBasedMatchHelper.h"
#import "IntroductionCollectionViewCell.h"
@interface InstructionViewController ()

@end

@implementation InstructionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    _introductionPageView.numberOfPages = 7;
    _introuductionArray = [[NSArray alloc]initWithObjects:@"help_screen1.png",@"help_screen2.png",@"help_screen3.png",@"help_screen4.png",@"help_screen5.png",@"help_screen6.png",@"help_screen7.png", nil];
    
    _descriptionArray = [[NSArray alloc]initWithObjects:@"To add any 3 numbers together horizontally, vertically, or diagonally to equal the Target.",@"Target: Prior to a game beginning, the Target is set. The Target is a number between 7 and 11. The Target starts at 7. Once the game with a Target of 7 is complete, a new game starts with a Target of 8.  This continues for a total of 5 games (7 through 11).",@"A player (or computer) places any number (1 through 5) on the playing board. A number is placed on the playing board by tapping the number you want to play and then tapping the space on the playing board where you would like to position the number.",@"The second player (or computer) places a number (1 through 5) on any open space on the playing board.",@"Players alternate turns by placing their remaining numbers on any open position on the playing board. Each number (1 through 5) can be played only once by each player.",@"Play continues until a winner is determined by achieving the Target by adding any 3 numbers together (horizontally, vertically, or diagonally) – you may use the numbers you played as well as those your opponent played to achieve the Target.",@"The player to place the last number resulting in the Target being achieved is the winner.The Target can only be achieved by adding 3 numbers together (not 2).A tie results if all positions on the playing board are filled without the Target being achieved.", nil];
    
    _headingArray = [[NSArray alloc]initWithObjects:@"Object",@"Target",@"Gameplay",@"",@"",@"",@"", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section        {
    return _introuductionArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath     {
    IntroductionCollectionViewCell* cell = [_introductionCollectionVIew dequeueReusableCellWithReuseIdentifier:@"IntroductionCollectionViewCell" forIndexPath:indexPath];
    cell.introductionImageView.image = [UIImage imageNamed:[_introuductionArray objectAtIndex:indexPath.row]];
    cell.descriptionTextBox.text = [_descriptionArray objectAtIndex:indexPath.row];
    cell.headingTextLabel.text = [_headingArray objectAtIndex:indexPath.row];
    
    switch (indexPath.row) {
        case 0:
            cell.headingTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:15];
            cell.descriptionTextBox.font = [UIFont fontWithName:@"gloriahallelujah" size:13];
            cell.headingTextLabel.frame = CGRectMake(cell.headingTextLabel.frame.origin.x, 256, cell.headingTextLabel.frame.size.width, cell.headingTextLabel.frame.size.height);
            cell.descriptionTextBox.frame = CGRectMake(cell.descriptionTextBox.frame.origin.x, 270, cell.descriptionTextBox.frame.size.width, cell.descriptionTextBox.frame.size.height);
            break;
        case 1:
            cell.headingTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:15];
            cell.descriptionTextBox.font = [UIFont fontWithName:@"gloriahallelujah" size:10];
            cell.headingTextLabel.frame = CGRectMake(cell.headingTextLabel.frame.origin.x, 240, cell.headingTextLabel.frame.size.width, cell.headingTextLabel.frame.size.height);
            cell.descriptionTextBox.frame = CGRectMake(cell.descriptionTextBox.frame.origin.x, 252, cell.descriptionTextBox.frame.size.width, cell.descriptionTextBox.frame.size.height);

            break;
        case 2:
            cell.headingTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:15];
            cell.descriptionTextBox.font = [UIFont fontWithName:@"gloriahallelujah" size:12];
            cell.headingTextLabel.frame = CGRectMake(cell.headingTextLabel.frame.origin.x, 238, cell.headingTextLabel.frame.size.width, cell.headingTextLabel.frame.size.height);
            cell.descriptionTextBox.frame = CGRectMake(cell.descriptionTextBox.frame.origin.x, 248, cell.descriptionTextBox.frame.size.width, cell.descriptionTextBox.frame.size.height);
            
            break;
        case 3:
            cell.headingTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:15];
            cell.descriptionTextBox.font = [UIFont fontWithName:@"gloriahallelujah" size:13];
            cell.descriptionTextBox.frame = CGRectMake(cell.descriptionTextBox.frame.origin.x, 255, cell.descriptionTextBox.frame.size.width, cell.descriptionTextBox.frame.size.height);
            break;
        case 4:
            cell.headingTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:15];
            cell.descriptionTextBox.font = [UIFont fontWithName:@"gloriahallelujah" size:13];
            cell.descriptionTextBox.frame = CGRectMake(cell.descriptionTextBox.frame.origin.x, 235, cell.descriptionTextBox.frame.size.width, cell.descriptionTextBox.frame.size.height);

            break;
        case 5:
            cell.headingTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:15];
            cell.descriptionTextBox.font = [UIFont fontWithName:@"gloriahallelujah" size:12];
            cell.descriptionTextBox.frame = CGRectMake(cell.descriptionTextBox.frame.origin.x, 235, cell.descriptionTextBox.frame.size.width, cell.descriptionTextBox.frame.size.height);

            break;
        case 6:
            cell.headingTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:15];
            cell.descriptionTextBox.font = [UIFont fontWithName:@"gloriahallelujah" size:11];
            cell.descriptionTextBox.frame = CGRectMake(cell.descriptionTextBox.frame.origin.x, 235, cell.descriptionTextBox.frame.size.width, cell.descriptionTextBox.frame.size.height);
            break;

        default:
            break;
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{


}

#pragma mark -ScrollDelegateMethod
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
        
    CGRect visibleRect = (CGRect){.origin = _introductionCollectionVIew.contentOffset, .size = _introductionCollectionVIew.bounds.size};
    CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
    NSIndexPath *visibleIndexPath = [_introductionCollectionVIew indexPathForItemAtPoint:visiblePoint];
    _introductionPageView.currentPage =(int)visibleIndexPath.row;
    int currentPage = (int)visibleIndexPath.row;
    NSLog(@"%i", currentPage);
    
    if (currentPage == 6) {
        _skipButton.hidden = YES;
        _startButton.hidden = NO;
    }else{
        _skipButton.hidden = NO;
        _startButton.hidden = YES;
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}

- (IBAction)startButtonClicked:(id)sender {
    NSString *string = [[NSUserDefaults standardUserDefaults]objectForKey:@"First Run"];
    if (string == nil) {
        [[GCTurnBasedMatchHelper sharedInstance] authenticateLocalUser];
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"Not First Load" forKey:@"First Run"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)skipButtonClicked:(id)sender {
    NSString *string = [[NSUserDefaults standardUserDefaults]objectForKey:@"First Run"];
    if (string == nil) {
        [[GCTurnBasedMatchHelper sharedInstance] authenticateLocalUser];
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"Not First Load" forKey:@"First Run"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.navigationController popToRootViewControllerAnimated:NO];

}
@end