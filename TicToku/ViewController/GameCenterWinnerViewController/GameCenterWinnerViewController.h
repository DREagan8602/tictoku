//
//  GameCenterWinnerViewController.h
//  TicTac
//
//  Created by Raza Najam on 5/11/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameCenterWinnerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *winOrLossImageView;
@property (weak, nonatomic) IBOutlet UILabel *playerOneNameTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerTwoTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerOneNumberOfWinningLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerTwoNumberOfWinningLabel;
@property (nonatomic, retain) NSDictionary *winnerDict;
@property (nonatomic, retain) NSDictionary *userDict;

@property (nonatomic, retain) NSString *playerOneName;
@property (nonatomic, retain) NSString *playerTwoName;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

- (IBAction)backButtonClicked:(id)sender;
- (IBAction)shareButtonClicked:(id)sender;
@end
