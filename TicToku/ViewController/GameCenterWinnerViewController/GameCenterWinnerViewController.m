//
//  GameCenterWinnerViewController.m
//  TicTac
//
//  Created by Raza Najam on 5/11/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "GameCenterWinnerViewController.h"
#import <Social/Social.h>

@interface GameCenterWinnerViewController ()

@end

@implementation GameCenterWinnerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@", _userDict);
    NSLog(@"%@", _winnerDict);
    [self updateScreenScore];
}

-(void)updateScreenScore    {

    int player1 = 0;
    int player2 = 0;
    for(NSString *key in [_winnerDict allKeys]) {
        if([[_winnerDict objectForKey:key]isEqualToString:[_userDict objectForKey:@"playerOneID"]]){
            player1++;
        }else{
            player2++;
        }
    }
    _playerOneNumberOfWinningLabel.text = [NSString stringWithFormat:@"%i", player1];
    _playerTwoNumberOfWinningLabel.text = [NSString stringWithFormat:@"%i", player2];
    _playerOneNameTextLabel.text = [_userDict objectForKey:@"playerOneName"];
    _playerTwoTextLabel.text =  [_userDict objectForKey:@"playerTwoName"];

    if (player1 > player2) {
        _winOrLossImageView.image = [UIImage imageNamed:@"YouWonGame.png"];
    }else if(player1 == player2){
        _winOrLossImageView.image = [UIImage imageNamed:@"TieGameImage.png"];
    }else{
        _winOrLossImageView.image = [UIImage imageNamed:@"YouLooseGame.png"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)shareButtonClicked:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        // Initialize Compose View Controller
        SLComposeViewController *vc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        // Configure Compose View Controller
        [vc setInitialText:@"Testing"];
        [vc addImage:[self giveMeImageForSharing]];
        _closeButton.hidden=NO;
        _shareButton.hidden=NO;
        _playButton.hidden=NO;
        
        // Present Compose View Controller
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        NSString *message = @"It seems that we cannot talk to Facebook at the moment or you have not yet added your Facebook account to this device. Go to the Settings application to add your Facebook account to this device.";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

-(UIImage *)giveMeImageForSharing    {
    _closeButton.hidden=YES;
    _shareButton.hidden=YES;
    _playButton.hidden=YES;
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(window.bounds.size, NO, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(window.bounds.size);
    }
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, -window.bounds.origin.x, -window.bounds.origin.y);
    [self.view.layer renderInContext:ctx];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //    UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    return viewImage;
}




@end
