//
//  WinnerViewController.m
//  TicTac
//
//  Created by Raza Najam on 2/27/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "WinnerViewController.h"
#import "GameViewController.h"
#import <Social/Social.h>
@interface WinnerViewController ()

@end

@implementation WinnerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self calculatingMatchStatistics];
    _winningPlayerNameTextLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:30];
    
}

-(void)calculatingMatchStatistics   {
    int player1 = 0;
    int player2 = 0;
    for(NSNumber *key in [_winnerDict allKeys]) {
        if([[_winnerDict objectForKey:key]isEqualToString:@"1"]){
            player1++;
        }else{
            player2++;
        }
    }
    _playerOneNumberOfWinningLabel.text = [NSString stringWithFormat:@"%i", player1];
    _playerTwoNumberOfWinningLabel.text = [NSString stringWithFormat:@"%i", player2];
    _playerOneNameTextLabel.text = _playerOneName;
    _playerTwoTextLabel.text = _playerTwoName;
    
    if (player1 > player2) {
        _winningPlayerNameTextLabel.text = _playerOneName;
    }else if(player1 == player2){
        _winningPlayerNameTextLabel.text = @"";
        _winOrLossImageView.image = [UIImage imageNamed:@"TieGameImage.png"];
    }else{
        _winningPlayerNameTextLabel.text = _playerTwoName;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)rematchButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)playAgainButtonClicked:(id)sender {
    [_winnerDict removeAllObjects];
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)shareButtonClicked:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        // Initialize Compose View Controller
        SLComposeViewController *vc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        // Configure Compose View Controller
        [vc setInitialText:@"Testing"];
        [vc addImage:[self giveMeImageForSharing]];
        _closeButton.hidden=NO;
        _shareButton.hidden=NO;
        _playButton.hidden=NO;

        // Present Compose View Controller
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        NSString *message = @"It seems that we cannot talk to Facebook at the moment or you have not yet added your Facebook account to this device. Go to the Settings application to add your Facebook account to this device.";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

-(UIImage *)giveMeImageForSharing    {
    _closeButton.hidden=YES;
    _shareButton.hidden=YES;
    _playButton.hidden=YES;
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(window.bounds.size, NO, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(window.bounds.size);
    }
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, -window.bounds.origin.x, -window.bounds.origin.y);
    [self.view.layer renderInContext:ctx];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
//    UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    return viewImage;
}



@end
