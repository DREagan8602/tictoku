//
//  WinnerViewController.h
//  TicTac
//
//  Created by Raza Najam on 2/27/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface WinnerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *winOrLossImageView;
@property (weak, nonatomic) IBOutlet UILabel *playerOneNameTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerTwoTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerOneNumberOfWinningLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerTwoNumberOfWinningLabel;
@property (weak, nonatomic) IBOutlet UIButton *playeAgainButton;
@property (nonatomic, retain) NSMutableDictionary *winnerDict;
@property (nonatomic, retain) NSString *playerOneName;
@property (nonatomic, retain) NSString *playerTwoName;

@property (weak, nonatomic) IBOutlet UILabel *winningPlayerNameTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;



- (IBAction)playAgainButtonClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;
- (IBAction)shareButtonClicked:(id)sender;
@end