//
//  SingleViewController.m
//  TicTac
//
//  Created by Raza Najam on 2/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "SingleViewController.h"
#import "GameViewController.h"
#define IS_IPHONE_4 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )480 ) < DBL_EPSILON )

@interface SingleViewController ()

@end

@implementation SingleViewController

#pragma mark - Methods -
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = YES;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self managingUI];
    _playerOneImageView.transform = CGAffineTransformMakeRotation(-.0550);
    _playerNameTextField.font = [UIFont fontWithName:@"gloriahallelujah" size:12];
    _playerNameTextField.delegate = self;
    _playerNameTextField.returnKeyType = UIReturnKeyDone;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)startButtonClicked:(id)sender {
    GameViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GameViewController"];
    viewController.playerOneImage = _playerOneImageView.image;
    viewController.playerOneName = _playerNameTextField.text;
    viewController.gamePlayer = 1;
    viewController.playerTwoName = @"Professor";
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)imageButtonClicked:(id)sender {
    _imagePickerController = [[UIImagePickerController alloc]init];
    _imagePickerController.delegate = self;
    _imagePickerController.allowsEditing = YES;
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *img = info[UIImagePickerControllerEditedImage];
    _playerOneImageView.image = img;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)managingUI       {
//    NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:17.0]
    if ([_playerNameTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor lightGrayColor];
        _playerNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Player Name" attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    if IS_IPHONE_4 {
        
    }
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

@end
