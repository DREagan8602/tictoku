//
//  SingleViewController.h
//  TicTac
//
//  Created by Raza Najam on 2/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *playerOneImageView;
@property (retain, nonatomic) UIImagePickerController *imagePickerController;
@property (weak, nonatomic) IBOutlet UITextField *playerNameTextField;
@property (weak, nonatomic) IBOutlet UIView *optionView;

- (IBAction)startButtonClicked:(id)sender;
- (IBAction)imageButtonClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;
@end
