//
//  TwoPlayerViewController.m
//  TicTac
//
//  Created by Raza Najam on 4/9/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "TwoPlayerViewController.h"
#import "GameCenterWinnerViewController.h"
#import "CustomAlertView.h"
#import "Shared.h"
@implementation NSArray (Random)

- (id) randomObject
{
    if ([self count] == 0) {
        return nil;
    }
    return [self objectAtIndex: arc4random() % [self count]];
}

@end

@interface TwoPlayerViewController ()

@end

@implementation TwoPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _playerOneNameLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:14];
    _playerTwoNameLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:14];
    _headerPlayerOneNameLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:14];
    _headerPlayerTwoNameLabel.font = [UIFont fontWithName:@"gloriahallelujah" size:14];

    if (_commonMainMutableDict == nil) {
        _commonMainMutableDict = [[NSMutableDictionary alloc]init];
    }
    if (_commonMutableArray == nil) {
        _commonMutableArray = [[NSMutableArray alloc]init];
    }
    
    [GCTurnBasedMatchHelper sharedInstance].delegate = self;
    self.navigationController.navigationBarHidden = YES;
    currentTargetedGame = 7;
    checkWinner = true;
    //_targetGameNumber = 7;
    [self checkCurrentMatchStatus];
    [self givingPlayerName];
    if ([[ UIScreen mainScreen ] bounds ].size.height>480)  {
        _numbView.frame = CGRectMake(_numbView.frame.origin.x, 430, _numbView.frame.size.width, _numbView.frame.size.height);
        _topBrdBtnView.frame = CGRectMake(_topBrdBtnView.frame.origin.x, 107, _topBrdBtnView.frame.size.width, _topBrdBtnView.frame.size.height);
        _brdCustView.frame = CGRectMake(_brdCustView.frame.origin.x, 162, _brdCustView.frame.size.width, _brdCustView.frame.size.height);
    }
}

-(void)viewDidAppear:(BOOL)animated{
    //    [self resetGame];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)givingPlayerName {
    NSString *playerOneDisplayName;
    NSString *playerTwoDisplayName;
    NSString *player1ID = [GKLocalPlayer localPlayer].playerID;
    NSString *player2ID;
    playerOneDisplayName = [GKLocalPlayer localPlayer].alias;
    _playerOneNameLabel.text = playerOneDisplayName;
    GKTurnBasedMatch *currentMatch = [[GCTurnBasedMatchHelper sharedInstance] currentMatch];
    NSUInteger currentIndex = [currentMatch.participants indexOfObject:currentMatch.currentParticipant];
    GKTurnBasedParticipant *nextParticipant;
    NSUInteger nextIndex = (currentIndex) % [currentMatch.participants count];
    nextParticipant = [currentMatch.participants objectAtIndex:nextIndex];
    playerTwoDisplayName = nextParticipant.player.alias;
    player2ID = nextParticipant.player.playerID;
    GKPlayer *player2 = nextParticipant.player;
    if ([player1ID isEqualToString:player2.playerID]) {
        NSUInteger currentIndex = [currentMatch.participants indexOfObject:currentMatch.currentParticipant];
        GKTurnBasedParticipant *nextParticipant;
        NSUInteger nextIndex = (currentIndex+1) % [currentMatch.participants count];
        nextParticipant = [currentMatch.participants objectAtIndex:nextIndex];
        GKPlayer *player2 = nextParticipant.player;
        player2ID = nextParticipant.player.playerID;
        playerTwoDisplayName = player2.alias;
    }
    _headerPlayerOneNameLabel.text = playerOneDisplayName;
    _headerPlayerTwoNameLabel.text = playerTwoDisplayName;
}

-(NSDictionary *)getUserData {
    NSString *playerOneDisplayName;
    NSString *playerTwoDisplayName;
    NSString *player1ID = [GKLocalPlayer localPlayer].playerID;
    NSString *player2ID;
    playerOneDisplayName = [GKLocalPlayer localPlayer].alias;
    _playerOneNameLabel.text = playerOneDisplayName;
    GKTurnBasedMatch *currentMatch = [[GCTurnBasedMatchHelper sharedInstance] currentMatch];
    NSUInteger currentIndex = [currentMatch.participants indexOfObject:currentMatch.currentParticipant];
    GKTurnBasedParticipant *nextParticipant;
    NSUInteger nextIndex = (currentIndex) % [currentMatch.participants count];
    nextParticipant = [currentMatch.participants objectAtIndex:nextIndex];
    playerTwoDisplayName = nextParticipant.player.alias;
    player2ID = nextParticipant.player.playerID;
    GKPlayer *player2 = nextParticipant.player;
    if ([player1ID isEqualToString:player2.playerID]) {
        NSUInteger currentIndex = [currentMatch.participants indexOfObject:currentMatch.currentParticipant];
        GKTurnBasedParticipant *nextParticipant;
        NSUInteger nextIndex = (currentIndex+1) % [currentMatch.participants count];
        nextParticipant = [currentMatch.participants objectAtIndex:nextIndex];
        GKPlayer *player2 = nextParticipant.player;
        player2ID = nextParticipant.player.playerID;
        playerTwoDisplayName = player2.alias;
    }
    _headerPlayerOneNameLabel.text = playerOneDisplayName;
    _headerPlayerTwoNameLabel.text = playerTwoDisplayName;
    
    NSDictionary *userDict = [[NSDictionary alloc]initWithObjectsAndKeys:playerOneDisplayName,@"playerOneName",playerTwoDisplayName,@"playerTwoName",player1ID,@"playerOneID",player2ID,@"PlayerTwoID", nil];
    return userDict;
}

-(void)checkCurrentMatchStatus  {
    
    GKTurnBasedMatch *currentMatch = [[GCTurnBasedMatchHelper sharedInstance] currentMatch];
    if (currentMatch.status ==2) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        [Shared showAlert:@"Game ended"];
        return;
    }
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:currentMatch.matchData];
    NSDictionary *myDictionaryMain = [unarchiver decodeObjectForKey:@"commonDict"];
    [unarchiver finishDecoding];
    if (!([[myDictionaryMain objectForKey:@"Winner"] isEqualToString:@"No Winner"]) &&(myDictionaryMain != nil) ) {
        NSString *winnerID = [myDictionaryMain objectForKey:@"Winner"];
        NSString *currentPlayerID = currentMatch.currentParticipant.playerID;
        if(![winnerID isEqualToString:currentPlayerID]){
            [self performSelector:@selector(YouLossView) withObject:nil afterDelay:1.5];
            int winningLine = [[myDictionaryMain objectForKey:@"WinningLine"]intValue];
            [self presentingLineOnGame:winningLine :winningLine];
        }
        _targetGameNumber = [[myDictionaryMain objectForKey:@"TargetGame"]intValue];
        if (_targetGameNumber == 11) {
            [self numberOfWinningGames:[myDictionaryMain objectForKey:@"Statistics"]];
        }
    }else if (([[myDictionaryMain objectForKey:@"Winner"] isEqualToString:@"Draw"]) &&(myDictionaryMain != nil) ) {
        [self performSelector:@selector(matchDraw) withObject:nil afterDelay:0];
        _targetGameNumber = [[myDictionaryMain objectForKey:@"TargetGame"]intValue];
        if (_targetGameNumber == 11) {
            [self showWinnerScreen:[myDictionaryMain objectForKey:@"Statistics"]];
        }
    }
    
    if (myDictionaryMain == nil) {
        _targetGameNumber = 7;
        _playerTurnImageView.hidden = NO;
    }else{
        _targetGameNumber = [[myDictionaryMain objectForKey:@"TargetGame"]intValue];
        UIButton *btn = [[UIButton alloc]init];
        btn.tag = _targetGameNumber;
        [self targetButtonClicked:btn];
    }
    NSDictionary *myDictionary = [myDictionaryMain objectForKey:@"Players"];
    NSLog(@"Player 2 %@", myDictionary);
    NSString *player1ID = [GKLocalPlayer localPlayer].playerID;
    NSDictionary *player1Dict = [myDictionary objectForKey:player1ID];
    NSDictionary *player2Dict;
    for(NSString *playerID in myDictionary.allKeys){
        if (![playerID isEqualToString:player1ID]) {
            player2Dict = [myDictionary objectForKey:playerID];
        }
    }
    
    for(NSString *numberPlacement in player1Dict){
        [self settingValueOnBoardPlayerOne:numberPlacement :[player1Dict objectForKey:numberPlacement]];
    }
    for(NSString *numberPlacement in player2Dict){
        [self settingValueOnBoardPlayerTwo:numberPlacement :[player2Dict objectForKey:numberPlacement]];
    }
    playedValues = player1Dict.allKeys.count+player2Dict.allKeys.count;
    if (![player1ID isEqualToString:currentMatch.currentParticipant.playerID]) {
        _boardView.userInteractionEnabled = NO;
    }
    if ([player1ID isEqualToString:currentMatch.currentParticipant.playerID]) {
        _playerTurnImageView.hidden = NO;
    }else{
        _playerTurnImageView.hidden = YES;
    }
}

-(void)YouLossView  {
    _custAlertView.hidden = NO;
    _custAlertTextLabel.text = @"";
    _winImageView.image = [UIImage imageNamed:@"YouLooseGame.png"];
    [self performSelectorOnMainThread:@selector(playLoosingSound) withObject:nil waitUntilDone:NO];
}

-(void)playLoosingSound {
    [Shared.instance startMusic:@"loosingSound"];
}

-(void)youWonView   {
    _custAlertView.hidden = NO;
    _custAlertTextLabel.text = @"";
    _winImageView.image = [UIImage imageNamed:@"winnerGameImage.png"];
    [Shared.instance startMusic:@"winningSound"];
}

-(void)matchDraw{
    _custAlertView.hidden = NO;
    _custAlertTextLabel.text = @"";
    _winImageView.image = [UIImage imageNamed:@"DrawGameImage.png"];

    
}
#pragma mark- Player Data...
-(NSDictionary *)getPlayerOneData:(NSMutableDictionary *)dict {
    NSDictionary *myDictionary = [dict objectForKey:@"Players"];
    NSString *player1ID = [GKLocalPlayer localPlayer].playerID;
    NSDictionary *player1Dict = [myDictionary objectForKey:player1ID];
    return player1Dict;
}

-(NSDictionary *)getPlayerTwoData:(NSMutableDictionary *)dict {
    NSDictionary *myDictionary = [dict objectForKey:@"Players"];
    NSString *player1ID = [GKLocalPlayer localPlayer].playerID;
    NSDictionary *player2Dict;
    for(NSString *playerID in myDictionary.allKeys){
        if (![playerID isEqualToString:player1ID]) {
            player2Dict = [myDictionary objectForKey:playerID];
        }
    }
    return player2Dict;
}

-(void)numberOfWinningGames:(NSDictionary *)dict    {
    NSString *player1ID = [GKLocalPlayer localPlayer].playerID;
    int playerOneWinning = 0;
    int playerTwoWinning = 0;
    for(NSString *strID in dict.allKeys)    {
        if ([[dict objectForKey:strID] isEqualToString:player1ID]) {
            playerOneWinning++;
        }else if(![[dict objectForKey:strID]isEqualToString:@"Draw"]){
            playerTwoWinning++;
        }
    }
    if (playerOneWinning > playerTwoWinning) {
        [self gameEnded:1];
    }else if(playerOneWinning == playerTwoWinning){
        [self gameEnded:3];
    }else{
        [self gameEnded:2];
    }
    [self showWinnerScreen:dict];
}

#pragma mark-GameCenter Handling...
- (void)gameEnded:(int)endingStatus   {
    GKTurnBasedMatch *currentMatch = [[GCTurnBasedMatchHelper sharedInstance] currentMatch];
    NSString *sendString = @"End";
    NSData *data = [sendString dataUsingEncoding:NSUTF8StringEncoding ];
    for (GKTurnBasedParticipant *part in currentMatch.participants) {
        if (endingStatus == 0) {
            part.matchOutcome = GKTurnBasedMatchOutcomeQuit;
        }else if(endingStatus == 1){
            part.matchOutcome = GKTurnBasedMatchOutcomeWon;
        }else if (endingStatus == 2){
            part.matchOutcome = GKTurnBasedMatchOutcomeLost;
        }
    }
    [currentMatch endMatchInTurnWithMatchData:data completionHandler:^(NSError *error) {
        if (error) {
            NSLog(@"%@", error);
        }
        
    }];
    NSLog(@"Game has ended");
    
}

#pragma mark - GCTurnBasedMatchHelperDelegate
-(void)enterNewGame:(GKTurnBasedMatch *)match {
    NSLog(@"Entering new game...");
    _targetGameNumber = 7;
}

-(void)layoutMatch:(GKTurnBasedMatch *)match {
    NSLog(@"Viewing match where it's not our turn...");
    NSString *statusString;
    
    if (match.status == GKTurnBasedMatchStatusEnded) {
        statusString = @"Match Ended";
    } else {
        int playerNum = [match.participants indexOfObject:match.currentParticipant] + 1;
        statusString = [NSString stringWithFormat:@"Player %d's Turn", playerNum];
    }
}

-(void)sendNotice:(NSString *)notice forMatch:(GKTurnBasedMatch *)match {
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"TicTac" message:notice delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [av show];
}

-(void)recieveEndGame:(GKTurnBasedMatch *)match {
    [self layoutMatch:match];
}

- (void)sendingDataClicked:(NSMutableDictionary *)dict {
    GKTurnBasedMatch *currentMatch = [[GCTurnBasedMatchHelper sharedInstance] currentMatch];
    NSMutableData *data = [[NSMutableData alloc]init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:dict forKey:@"commonDict"];
    [archiver finishEncoding];
    NSUInteger currentIndex = [currentMatch.participants indexOfObject:currentMatch.currentParticipant];
    GKTurnBasedParticipant *nextParticipant;
    NSUInteger nextIndex = (currentIndex + 1) % [currentMatch.participants count];
    nextParticipant = [currentMatch.participants objectAtIndex:nextIndex];
    for (int i = 0; i < [currentMatch.participants count]; i++) {
        nextParticipant = [currentMatch.participants objectAtIndex:((currentIndex + 1 + i) % [currentMatch.participants count ])];
        if (nextParticipant.matchOutcome != GKTurnBasedMatchOutcomeQuit) {
            //NSLog(@"isnt' quit %@", nextParticipant);
            break;
        } else if(nextParticipant.matchOutcome == GKTurnBasedMatchOutcomeQuit) {
            GKTurnBasedParticipant *next = [currentMatch.participants objectAtIndex:(currentIndex + 1)%[currentMatch.participants count]];
            [currentMatch participantQuitInTurnWithOutcome:GKTurnBasedMatchOutcomeQuit nextParticipants:@[next] turnTimeout:MAXFLOAT matchData:currentMatch.matchData completionHandler:nil];
            [next setMatchOutcome:GKTurnBasedMatchOutcomeWon];
            [currentMatch endMatchInTurnWithMatchData:currentMatch.matchData completionHandler:nil];
        }
    }
    
    [currentMatch endTurnWithNextParticipant:nextParticipant matchData:data completionHandler:^(NSError *error) {
        if (error) {
            NSLog(@"PROBLEM OCCURED");
        } else {
            _boardView.userInteractionEnabled = NO;
            NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
            NSDictionary *myDictionary = [unarchiver decodeObjectForKey:@"commonDict"];
            [unarchiver finishDecoding];
            NSLog(@"Player 2 %@", myDictionary);
        }
    }];
    //    NSLog(@"Send Turn, %@", nextParticipant);
}

-(void)takeTurn:(GKTurnBasedMatch *)match {
    _boardView.userInteractionEnabled = YES;
    [self checkCurrentMatchStatus];
}

-(void) maintainingGameState:(NSDictionary *)dict    {
    //[GKLocalPlayer localPlayer].playerID]
    NSDictionary *commonDict = [dict objectForKey:@"Players"];
    NSDictionary *player1Dict = [commonDict objectForKey:[self getCurrentPlayerID]];
    NSDictionary *player2Dict;
    for(NSString *key in commonDict.allKeys){
        if (![key isEqualToString:[self getCurrentPlayerID]]) {
            player2Dict = [commonDict objectForKey:key];
        }
    }
    
    for(NSString *numberPlacement in player1Dict){
        [self settingValueOnBoardPlayerOne:numberPlacement :[player1Dict objectForKey:numberPlacement]];
    }
    
    for(NSString *numberPlacement in player2Dict){
        [self settingValueOnBoardPlayerTwo:numberPlacement :[player2Dict objectForKey:numberPlacement]];
    }
}

- (IBAction)boardButtonClicked:(id)sender {
    if (player1SelectedValue == 0) {
        return;
    }
    UIButton *btn = (UIButton *)sender;
    playedValues++;
    NSLog(@"%d", player1SelectedValue);
    NSString *imgName = [NSString stringWithFormat:@"boardP1Number%ld", (long)player1SelectedValue];
    [btn setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    [_player1ValuesDict setObject:[NSNumber numberWithInt:player1SelectedValue] forKey:[NSString stringWithFormat:@"%ld",(long)[sender tag]]];
    btn.userInteractionEnabled = NO;
    _selectedBtn.hidden = YES;
    NSString *playedValueString = [NSString stringWithFormat:@"%ld", (long)player1SelectedValue];
    NSString *placementValue = [NSString stringWithFormat:@"%li", (long)[sender tag]];
    [self preparingDataToSend:playedValueString :placementValue];
    _boardView.userInteractionEnabled = NO;
    player1SelectedValue = 0;
    _playerTurnImageView.hidden = YES;
    
}

-(void)preparingDataToSend:(NSString *)playedValueString :(NSString *)boardPlacementValue {
    GKTurnBasedMatch *currentMatch = [[GCTurnBasedMatchHelper sharedInstance] currentMatch];
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:currentMatch.matchData];
    NSMutableDictionary *mainPlayerDict = [unarchiver decodeObjectForKey:@"commonDict"];
    [unarchiver finishDecoding];
    NSMutableDictionary *playerDict;
    NSMutableDictionary *playerMutableDictionary;
    NSMutableDictionary *gameStatisticsMutableDictionary;
    if (mainPlayerDict == nil) {
        mainPlayerDict = [[NSMutableDictionary alloc]init];
        playerDict = [[NSMutableDictionary alloc]init];
        playerMutableDictionary = [[NSMutableDictionary alloc]init];
        gameStatisticsMutableDictionary = [[NSMutableDictionary alloc]init];
    }else{
        playerDict = [[mainPlayerDict objectForKey:@"Players"]mutableCopy];
        gameStatisticsMutableDictionary = [[mainPlayerDict objectForKey:@"Statistics"]mutableCopy];
        if (gameStatisticsMutableDictionary == nil) {
            gameStatisticsMutableDictionary = [[NSMutableDictionary alloc]init];
        }
        NSDictionary *playerValueDictionary = [playerDict objectForKey:[self getCurrentPlayerID]];
        if (playerValueDictionary == nil) {
            playerMutableDictionary = [[NSMutableDictionary alloc]init];
        }else{
            playerMutableDictionary = [playerValueDictionary mutableCopy];
        }
    }
    
    NSString *currentTarget = [NSString stringWithFormat:@"%i", _targetGameNumber];
    if (![[mainPlayerDict objectForKey:@"TargetGame"] isEqualToString:currentTarget]) {
        [playerMutableDictionary removeAllObjects];
        [playerDict removeAllObjects];
        [playerMutableDictionary setObject:playedValueString forKey:boardPlacementValue];
        [playerDict setObject:playerMutableDictionary forKey:[self getCurrentPlayerID]];
        NSString *targetGame = [NSString stringWithFormat:@"%i", _targetGameNumber];
        [mainPlayerDict setObject:targetGame forKey:@"TargetGame"];
        [mainPlayerDict setObject:@"No Winner" forKey:@"Winner"];
        [mainPlayerDict setObject:@"0" forKey:@"WinningLine"];
        [mainPlayerDict setObject:gameStatisticsMutableDictionary forKey:@"Statistics"];
        [mainPlayerDict setObject:playerDict forKey:@"Players"];
        [self sendingDataClicked:mainPlayerDict];
    }else{
        
        [playerMutableDictionary setObject:playedValueString forKey:boardPlacementValue];
        [playerDict setObject:playerMutableDictionary forKey:[self getCurrentPlayerID]];
        NSString *targetGame = [NSString stringWithFormat:@"%i", _targetGameNumber];
        [mainPlayerDict setObject:targetGame forKey:@"TargetGame"];
        [mainPlayerDict setObject:@"No Winner" forKey:@"Winner"];
        [mainPlayerDict setObject:@"0" forKey:@"WinningLine"];
        [mainPlayerDict setObject:gameStatisticsMutableDictionary forKey:@"Statistics"];
        [mainPlayerDict setObject:playerDict forKey:@"Players"];
        [self checkWinnerOfGame:mainPlayerDict];
    }
}

//Getting current player ID...
-(NSString *)getCurrentPlayerID  {
    GKTurnBasedMatch *currentMatch = [[GCTurnBasedMatchHelper sharedInstance] currentMatch];
    NSString *playerID = currentMatch.currentParticipant.playerID;
    return playerID;
    
}

- (IBAction)player1NumbersClicked:(id)sender    {
    if (checkWinner) {
        _selectedBtn = (UIButton *)sender;
        player1SelectedValue = [sender tag];
        [self selectionofPlayerOneButton:[sender tag]];
    }
}

//Checking winner...
-(void)checkWinnerOfGame:(NSMutableDictionary *)dictToSend
{
    NSString *winningLine;
    NSString *player1ID = [GKLocalPlayer localPlayer].playerID;
    BOOL winner =false;
    NSDictionary *player2ValuesDict = [self getPlayerTwoData:dictToSend];
    NSDictionary *player1ValuesDict = [self getPlayerOneData:dictToSend];
    
    int row1col1Player2 =  [[player2ValuesDict objectForKey:@"1"] intValue];
    int row1col2Player2 =  [[player2ValuesDict objectForKey:@"2"] intValue];
    int row1col3Player2 =  [[player2ValuesDict objectForKey:@"3"] intValue];
    int row2col1Player2 =  [[player2ValuesDict objectForKey:@"4"] intValue];
    int row2col2Player2 =  [[player2ValuesDict objectForKey:@"5"] intValue];
    int row2col3Player2 =  [[player2ValuesDict objectForKey:@"6"] intValue];
    int row3col1Player2 =  [[player2ValuesDict objectForKey:@"7"] intValue];
    int row3col2Player2 =  [[player2ValuesDict objectForKey:@"8"] intValue];
    int row3col3Player2 =  [[player2ValuesDict objectForKey:@"9"] intValue];
    
    int row1col1 =  [[player1ValuesDict objectForKey:@"1"] intValue];
    int row1col2 =  [[player1ValuesDict objectForKey:@"2"] intValue];
    int row1col3 =  [[player1ValuesDict objectForKey:@"3"] intValue];
    int row2col1 =  [[player1ValuesDict objectForKey:@"4"] intValue];
    int row2col2 =  [[player1ValuesDict objectForKey:@"5"] intValue];
    int row2col3 =  [[player1ValuesDict objectForKey:@"6"] intValue];
    int row3col1 =  [[player1ValuesDict objectForKey:@"7"] intValue];
    int row3col2 =  [[player1ValuesDict objectForKey:@"8"] intValue];
    int row3col3 =  [[player1ValuesDict objectForKey:@"9"] intValue];
    int calculateValue;
    
    if ((row1col1 != 0||row1col1Player2!= 0) && (row1col2!= 0|| row1col2Player2!= 0) && (row1col3 != 0 || row1col3Player2!= 0)) {
        calculateValue  = row1col1+row1col2+row1col3+row1col1Player2+row1col2Player2+row1col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:1 :1];
            winner = true;
            winningLine = @"1";
        }
    }
    if((row2col1 != 0|| row2col1Player2!= 0) && (row2col2 != 0||row2col2Player2 ) && (row2col3 != 0||row2col3Player2 != 0 )) {
        calculateValue  = row2col1+row2col2+row2col3+row2col1Player2+row2col2Player2+row2col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:2 :1];
            winner = true;
            winningLine = @"2";
        }
    }
    if((row3col1 != 0 || row3col1Player2 != 0) && (row3col2 !=0 || row3col2Player2 !=0 ) && (row3col3 != 0 || row3col3Player2 !=0 )) {
        calculateValue  = row3col1+row3col2+row3col3+row3col1Player2+row3col2Player2+row3col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:3 :1];
            winner = true;
            winningLine = @"3";
        }
    }
    if((row1col1 != 0 || row1col1Player2 != 0) && (row2col1 != 0 || row2col1Player2 !=0) && (row3col1 != 0 ||row3col1Player2 !=0)) {
        calculateValue  = row1col1+row2col1+row3col1 + row1col1Player2+row2col1Player2+row3col1Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:4 :1];
            winner = true;
            winningLine = @"4";
        }
    }
    if((row1col2 != 0 || row1col2Player2 != 0) && (row2col2 != 0 || row2col2Player2 != 0) && (row3col2 != 0 ||row3col2Player2 != 0)) {
        calculateValue  = row1col2+row2col2+row3col2+row1col2Player2+row2col2Player2+row3col2Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:5 :1];
            winner = true;
            winningLine = @"5";
        }
    }
    if((row1col3 != 0 || row1col3Player2 != 0) && (row2col3 != 0 || row2col3Player2 != 0) && (row3col3 != 0 || row3col3Player2 != 0)) {
        calculateValue  = row1col3+row2col3+row3col3+row1col3Player2+row2col3Player2+row3col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:6 :1];
            winner = true;
            winningLine = @"6";
        }
    }
    if((row1col1 != 0 || row1col1Player2 != 0) && (row2col2 != 0|| row2col2Player2 != 0) && (row3col3 != 0 || row3col3Player2 != 0)) {
        calculateValue  = row1col1+row2col2+row3col3+row1col1Player2+row2col2Player2+row3col3Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:7 :1];
            winningLine = @"7";
            winner = true;
        }
    }
    if((row1col3 != 0 || row1col3Player2 != 0) && (row2col2 != 0 || row2col2Player2 != 0) && (row3col1 != 0 || row3col1Player2 != 0)) {
        calculateValue  = row1col3+row2col2+row3col1+row1col3Player2+row2col2Player2+row3col1Player2;
        if (calculateValue == _targetGameNumber) {
            [self presentingLineOnGame:8 :1];
            winner = true;
            winningLine = @"8";
        }
    }
    if (winner) {
        [self performSelector:@selector(youWonView) withObject:nil afterDelay:1.5];
        NSString *gameNumber = [NSString stringWithFormat:@"%i", _targetGameNumber];
        NSMutableDictionary *statisticsMutable = [[dictToSend objectForKey:@"Statistics"] mutableCopy];
        [statisticsMutable setObject:player1ID forKey:gameNumber];
        [dictToSend setObject:player1ID forKey:@"Winner"];
        [dictToSend setObject:winningLine forKey:@"WinningLine"];
        [dictToSend setObject:statisticsMutable forKey:@"Statistics"];
    }else{
        if (playedValues >= 9) {
            if(_targetGameNumber >= 11) {
                [self gameEnded:0];
                _custAlertTextLabel.text = @"Draw";
                _custAlertView.hidden = NO;
                NSString *gameNumber = [NSString stringWithFormat:@"%i", _targetGameNumber];
                NSMutableDictionary *statisticsMutable = [[dictToSend objectForKey:@"Statistics"] mutableCopy];
                [statisticsMutable setObject:@"0" forKey:gameNumber];
                [dictToSend setObject:@"Draw" forKey:@"Winner"];
                [dictToSend setObject:0 forKey:@"WinningLine"];
                [dictToSend setObject:statisticsMutable forKey:@"Statistics"];
                [self showWinnerScreen:[dictToSend objectForKey:@"Statistics"]];
            }else{
                NSString *gameNumber = [NSString stringWithFormat:@"%i", _targetGameNumber];
                NSMutableDictionary *statisticsMutable = [[dictToSend objectForKey:@"Statistics"] mutableCopy];
                [statisticsMutable setObject:@"0" forKey:gameNumber];
                [dictToSend setObject:@"Draw" forKey:@"Winner"];
                [dictToSend setObject:0 forKey:@"WinningLine"];
                [dictToSend setObject:statisticsMutable forKey:@"Statistics"];
                [self performSelector:@selector(matchDraw) withObject:nil afterDelay:1];
            }
            _whoWinns = @"";
        }
    }
    if (winner) {
        if (_targetGameNumber == 11) {
            [self showWinnerScreen:[dictToSend objectForKey:@"Statistics"]];
        }
    }
    [self sendingDataClicked:dictToSend];
}
/*
 -(void)displayingAlert  {
 
 if (playerTurn == 2) {
 if (!(currentTargetedGame >= 5)) {
 _customView = [[[NSBundle mainBundle]loadNibNamed:@"CustomAlertView" owner:self options:nil]objectAtIndex:0];
 _customView.center = self.view.center;
 [_customView.nextStageButton addTarget:self action:@selector(nextStageButtonClicked) forControlEvents:UIControlEventTouchUpInside];
 [_customView.playAgainButton addTarget:self action:@selector(playAgainButtonClicked) forControlEvents:UIControlEventTouchUpInside];
 [_customView.quitButton addTarget:self action:@selector(quitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
 _customView.winnerNameLabel.text = _playerOneName;
 [self.view addSubview:_customView];
 }else{
 [self showWinnerScreen];
 }
 }else{
 if (!(currentTargetedGame >= 5)) {
 
 _customView = [[[NSBundle mainBundle]loadNibNamed:@"CustomAlertView" owner:self options:nil]objectAtIndex:0];
 _customView.center = self.view.center;
 [_customView.nextStageButton addTarget:self action:@selector(nextStageButtonClicked) forControlEvents:UIControlEventTouchUpInside];
 [_customView.playAgainButton addTarget:self action:@selector(playAgainButtonClicked) forControlEvents:UIControlEventTouchUpInside];
 [_customView.quitButton addTarget:self action:@selector(quitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
 _customView.winnerNameLabel.text = _playerTwoName;
 [self.view addSubview:_customView];
 }else{
 [self showWinnerScreen];
 }
 }
 }
 */

-(void)presentingLineOnGame:(int)position :(int)player     {
    switch (position) {
        case 1:
            [self fadeInImage:_horizontal1ImageView];
            break;
        case 2:
            [self fadeInImage:_horizontal2ImageView];
            break;
        case 3:
            [self fadeInImage:_horizontal3ImageView];
            break;
        case 4:
            [self fadeInImage:_vertical1ImageView];
            break;
        case 5:
            [self fadeInImage:_vertical2ImageView];
            break;
        case 6:
            [self fadeInImage:_vertical3ImageView];
            break;
        case 7:
            [self fadeInImage:_diagnalLeftImageView];
            break;
        case 8:
            [self fadeInImage:_diagnalRightImageView];
            break;
        default:
            break;
    }
}

-(void)showWinnerScreen:(NSDictionary *)dict {
    currentTargetedGame=1;
    UIButton *btn = [[UIButton alloc]init];
    btn.tag = currentTargetedGame;
    [self targetButtonClicked:btn];
    
    GameCenterWinnerViewController *winner = [self.storyboard instantiateViewControllerWithIdentifier:@"GameCenterWinnerViewController"];
    winner.userDict = [self getUserData];
    winner.winnerDict = dict;
    [self.navigationController pushViewController:winner animated:NO];
}

-(void)fadeInImage:(UIImageView *)img{
    
    [UIView animateWithDuration:0.3
                          delay:0.3
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{ img.alpha = 1; }
                     completion:^(BOOL finished){}
     ];
    checkWinner = false;
}

-(void)showAler:(NSString *)messageString       {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Tic Toku" message:messageString delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
}

//Select Target game
- (IBAction)targetButtonClicked:(id)sender {
    
    switch ([sender tag]) {
        case 7:
            _targetNumber7Button.selected = true;
            _targetNumber8Button.selected = false;
            _targetNumber9Button.selected = false;
            _targetNumber10Button.selected = false;
            _targetNumber11Button.selected = false;
            _targetGameNumber = 7;
            break;
        case 8:
            _targetNumber7Button.selected = false;
            _targetNumber8Button.selected = true;
            _targetNumber9Button.selected = false;
            _targetNumber10Button.selected = false;
            _targetNumber11Button.selected = false;
            _targetGameNumber=8;
            break;
        case 9:
            _targetNumber7Button.selected = false;
            _targetNumber8Button.selected = false;
            _targetNumber9Button.selected = true;
            _targetNumber10Button.selected = false;
            _targetNumber11Button.selected = false;
            _targetGameNumber=9;
            break;
        case 10:
            _targetNumber7Button.selected = false;
            _targetNumber8Button.selected = false;
            _targetNumber9Button.selected = false;
            _targetNumber10Button.selected = true;
            _targetNumber11Button.selected = false;
            _targetGameNumber=10;
            break;
        case 11:
            _targetNumber7Button.selected = false;
            _targetNumber8Button.selected = false;
            _targetNumber9Button.selected = false;
            _targetNumber10Button.selected = false;
            _targetNumber11Button.selected = true;
            _targetGameNumber=11;
            break;
        default:
            break;
    }
    _targetNumberTextLabel.text = [NSString stringWithFormat:@"TARGET IS %d",_targetGameNumber];
}

//Selection of number by player 1...
-(void)selectionofPlayerOneButton:(int)selectedTag {
    
    switch (selectedTag) {
        case 1:
            _player1Number1Button.selected = true;
            _player1Number2Button.selected = false;
            _player1Number3Button.selected = false;
            _player1Number4Button.selected = false;
            _player1Number5Button.selected = false;
            break;
        case 2:
            _player1Number1Button.selected = false;
            _player1Number2Button.selected = true;
            _player1Number3Button.selected = false;
            _player1Number4Button.selected = false;
            _player1Number5Button.selected = false;
            break;
        case 3:
            _player1Number1Button.selected = false;
            _player1Number2Button.selected = false;
            _player1Number3Button.selected = true;
            _player1Number4Button.selected = false;
            _player1Number5Button.selected = false;
            break;
        case 4:
            _player1Number1Button.selected = false;
            _player1Number2Button.selected = false;
            _player1Number3Button.selected = false;
            _player1Number4Button.selected = true;
            _player1Number5Button.selected = false;
            break;
        case 5:
            _player1Number1Button.selected = false;
            _player1Number2Button.selected = false;
            _player1Number3Button.selected = false;
            _player1Number4Button.selected = false;
            _player1Number5Button.selected = true;
            break;
        default:
            break;
    }
}

-(void)resetGame {
    _player1Number1Button.hidden = NO;
    _player1Number2Button.hidden = NO;
    _player1Number3Button.hidden = NO;
    _player1Number4Button.hidden = NO;
    _player1Number5Button.hidden = NO;
    [_btn1 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn2 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn3 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn4 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn5 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn6 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn7 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn8 setBackgroundImage:nil forState:UIControlStateNormal];
    [_btn9 setBackgroundImage:nil forState:UIControlStateNormal];
    _btn1.userInteractionEnabled = YES;
    _btn2.userInteractionEnabled = YES;
    _btn3.userInteractionEnabled = YES;
    _btn4.userInteractionEnabled = YES;
    _btn5.userInteractionEnabled = YES;
    _btn6.userInteractionEnabled = YES;
    _btn7.userInteractionEnabled = YES;
    _btn8.userInteractionEnabled = YES;
    _btn9.userInteractionEnabled = YES;
    
    [_player1ValuesDict removeAllObjects];
    [_player2ValuesDict removeAllObjects];
    playerTurn=1;
    _diagnalRightImageView.alpha = 0;
    _diagnalLeftImageView.alpha = 0;
    _horizontal1ImageView.alpha = 0;
    _horizontal2ImageView.alpha = 0;
    _horizontal3ImageView.alpha = 0;
    _vertical1ImageView.alpha = 0;
    _vertical2ImageView.alpha = 0;
    _vertical3ImageView.alpha = 0;
    checkWinner = true;
    playedValues = 0;
    _player1Number1Button.selected = false;
    _player1Number2Button.selected = false;
    _player1Number3Button.selected = false;
    _player1Number4Button.selected = false;
    _player1Number5Button.selected = false;
}

#pragma mark - customViewAlert button...
-(void)nextStageButtonClicked   {
    currentTargetedGame++;
    UIButton *btn = [[UIButton alloc]init];
    btn.tag = currentTargetedGame;
    [self targetButtonClicked:btn];
    [self resetGame];
    [_customView removeFromSuperview];
    
}

-(void)playAgainButtonClicked   {
    [self resetGame];
    [_customView removeFromSuperview];
    
}

-(void)quitButtonClicked   {
    [self.navigationController popViewControllerAnimated:YES];
}

//////////////Disable board values and hide played numbers........
-(void)settingValueOnBoardPlayerOne:(NSString *)placement :(NSString *)gameNumber {
    int placementVal = [placement intValue];
    switch (placementVal) {
        case 1:{
            NSString *imgName = [NSString stringWithFormat:@"boardP1Number%@.png", gameNumber];
            [_btn1 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn1.userInteractionEnabled = NO;
        }
            break;
        case 2:{
            NSString *imgName = [NSString stringWithFormat:@"boardP1Number%@.png", gameNumber];
            [_btn2 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn2.userInteractionEnabled = NO;
        }
            break;
        case 3:{
            NSString *imgName = [NSString stringWithFormat:@"boardP1Number%@.png", gameNumber];
            [_btn3 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn3.userInteractionEnabled = NO;
        }
            break;
        case 4:{
            NSString *imgName = [NSString stringWithFormat:@"boardP1Number%@.png", gameNumber];
            [_btn4 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn4.userInteractionEnabled = NO;
        }
            break;
        case 5:{
            NSString *imgName = [NSString stringWithFormat:@"boardP1Number%@.png", gameNumber];
            [_btn5 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn5.userInteractionEnabled = NO;
        }
            break;
        case 6:{
            NSString *imgName = [NSString stringWithFormat:@"boardP1Number%@.png", gameNumber];
            [_btn6 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn6.userInteractionEnabled = NO;
        }
            break;
        case 7:{
            NSString *imgName = [NSString stringWithFormat:@"boardP1Number%@.png", gameNumber];
            [_btn7 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn7.userInteractionEnabled = NO;
        }
            break;
        case 8:{
            NSString *imgName = [NSString stringWithFormat:@"boardP1Number%@.png", gameNumber];
            [_btn8 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn8.userInteractionEnabled = NO;
        }
            break;
        case 9:{
            NSString *imgName = [NSString stringWithFormat:@"boardP1Number%@.png", gameNumber];
            [_btn9 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn9.userInteractionEnabled = NO;
        }
            break;
        default:
            break;
    }
    [self disablePlayedValues:gameNumber];
}

-(void)settingValueOnBoardPlayerTwo:(NSString *)placement :(NSString *)gameNumber {
    int placementVal = [placement intValue];
    switch (placementVal) {
        case 1:{
            NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@.png", gameNumber];
            [_btn1 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn1.userInteractionEnabled = NO;
        }
            break;
        case 2:{
            NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@.png", gameNumber];
            [_btn2 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn2.userInteractionEnabled = NO;
        }
            break;
        case 3:{
            NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@.png", gameNumber];
            [_btn3 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn3.userInteractionEnabled = NO;
        }
            break;
        case 4:{
            NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@.png", gameNumber];
            [_btn4 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn4.userInteractionEnabled = NO;
        }
            break;
        case 5:{
            NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@.png", gameNumber];
            [_btn5 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn5.userInteractionEnabled = NO;
        }
            break;
        case 6:{
            NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@.png", gameNumber];
            [_btn6 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn6.userInteractionEnabled = NO;
        }
            break;
        case 7:{
            NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@.png", gameNumber];
            [_btn7 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn7.userInteractionEnabled = NO;
        }
            break;
        case 8:{
            NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@.png", gameNumber];
            [_btn8 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn8.userInteractionEnabled = NO;
        }
            break;
        case 9:{
            NSString *imgName = [NSString stringWithFormat:@"boardP2Number%@.png", gameNumber];
            [_btn9 setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            _btn9.userInteractionEnabled = NO;
        }
            
            break;
            
        default:
            break;
    }
}


-(void)disablePlayedValues:(NSString *)playedValu  {
    int playedVal = [playedValu intValue];
    switch (playedVal) {
        case 1:
            _player1Number1Button.hidden = YES;
            break;
        case 2:
            _player1Number2Button.hidden = YES;
            break;
        case 3:
            _player1Number3Button.hidden = YES;
            break;
        case 4:
            _player1Number4Button.hidden = YES;
            break;
        case 5:
            _player1Number5Button.hidden = YES;
            break;
            
        default:
            break;
    }
}

- (IBAction)custAlertPlayNextButton:(id)sender {
    _custAlertView.hidden = YES;
    _targetGameNumber++;
    NSLog(@"%i", _targetGameNumber);
    UIButton *btn = [[UIButton alloc]init];
    btn.tag = _targetGameNumber;
    [self targetButtonClicked:btn];
    [self resetGame];
}

-(void)sendResetData  {
    /*
     GKTurnBasedMatch *currentMatch = [[GCTurnBasedMatchHelper sharedInstance] currentMatch];
     NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:currentMatch.matchData];
     NSMutableDictionary *mainDict = [unarchiver decodeObjectForKey:@"commonDict"];
     [unarchiver finishDecoding];
     NSDictionary *StatsDict = [mainDict objectForKey:@"Statistics"];
     NSMutableDictionary *mainPlayerDict = [[NSMutableDictionary alloc]init];
     NSMutableDictionary *playerDict = [[NSMutableDictionary alloc]init];
     NSMutableDictionary *playerMutableDictionary = [[NSMutableDictionary alloc]init];
     NSMutableDictionary *gameStatisticsMutableDictionary = [[NSMutableDictionary alloc]init];
     NSString *targetGame = [NSString stringWithFormat:@"%i", _targetGameNumber++];
     [mainPlayerDict setObject:targetGame forKey:@"TargetGame"];
     [mainPlayerDict setObject:@"No Winner" forKey:@"Winner"];
     [mainPlayerDict setObject:@"0" forKey:@"WinningLine"];
     [mainPlayerDict setObject:StatsDict forKey:@"Statistics"];
     [mainPlayerDict setObject:playerDict forKey:@"Players"];
     */
}

- (IBAction)custAlertQuitButton:(id)sender {
    [self gameEnded:0];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
