//
//  TwoPlayerViewController.h
//  TicTac
//
//  Created by Raza Najam on 4/9/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlertView.h"
#import "GCTurnBasedMatchHelper.h"

@interface NSArray (Random)
- (id) randomObject;
@end

@interface TwoPlayerViewController : UIViewController<GCTurnBasedMatchHelperDelegate>   {
int playerTurn;
int player1SelectedValue;
BOOL checkWinner;
int playedValues;
int currentTargetedGame;
}
@property (weak, nonatomic) IBOutlet UIView *brdCustView;

@property (weak, nonatomic) IBOutlet UIView *topBrdBtnView;

@property (weak, nonatomic) IBOutlet UIView *numbView;
@property (weak, nonatomic) IBOutlet UIImageView *winImageView;
@property (weak, nonatomic) IBOutlet UIView *boardView;
@property (weak, nonatomic) IBOutlet UIView *custAlertView;
@property (weak, nonatomic) IBOutlet UILabel *custAlertTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPlayerTwoNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerPlayerOneNameLabel;
@property (weak, nonatomic) CustomAlertView *customView;

@property (retain, nonatomic) NSString *whoWinns;
@property (retain, nonatomic) NSMutableArray *computerNumbers;
@property (retain, nonatomic) NSMutableArray *computerPlacements;

@property (retain, nonatomic) NSString *playerOneName;
@property (retain, nonatomic) NSString *playerTwoName;
@property (retain, nonatomic) UIImage *playerOneImage;
@property (retain, nonatomic) UIImage *playerTwoImage;
@property (assign) int targetGameNumber;
@property (assign) int gamePlayer;

@property (weak, nonatomic) IBOutlet UIImageView *player1ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *player2ImageView;
@property (weak, nonatomic) IBOutlet UILabel *targetNumberTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *diagnalLeftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *diagnalRightImageView;
@property (weak, nonatomic) IBOutlet UIImageView *horizontal1ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *horizontal2ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *horizontal3ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *vertical1ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *vertical2ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *vertical3ImageView;

@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIButton *btn7;
@property (weak, nonatomic) IBOutlet UIButton *btn8;
@property (weak, nonatomic) IBOutlet UIButton *btn9;

@property (weak, nonatomic) IBOutlet UIButton *player1Number1Button;
@property (weak, nonatomic) IBOutlet UIButton *player1Number2Button;
@property (weak, nonatomic) IBOutlet UIButton *player1Number3Button;
@property (weak, nonatomic) IBOutlet UIButton *player1Number4Button;
@property (weak, nonatomic) IBOutlet UIButton *player1Number5Button;

@property (retain, nonatomic) NSMutableDictionary *player1ValuesDict;
@property (retain, nonatomic) NSMutableDictionary *player2ValuesDict;
@property (retain, nonatomic) UIButton *selectedBtn;
@property (retain, nonatomic) NSMutableDictionary *commonMainMutableDict;
@property (retain, nonatomic) NSMutableArray *commonMutableArray;

@property (weak, nonatomic) IBOutlet UIButton *targetNumber7Button;
@property (weak, nonatomic) IBOutlet UIButton *targetNumber8Button;
@property (weak, nonatomic) IBOutlet UIButton *targetNumber9Button;
@property (weak, nonatomic) IBOutlet UIButton *targetNumber10Button;
@property (weak, nonatomic) IBOutlet UIButton *targetNumber11Button;

@property (weak, nonatomic) IBOutlet UIImageView *playerTurnImageView;

@property (weak, nonatomic) IBOutlet UILabel *playerOneNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerTwoNameLabel;

- (IBAction)targetButtonClicked:(id)sender;
- (IBAction)boardButtonClicked:(id)sender;
- (IBAction)player1NumbersClicked:(id)sender;
- (IBAction)custAlertPlayNextButton:(id)sender;
- (IBAction)custAlertQuitButton:(id)sender;

@end
