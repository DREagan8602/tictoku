//
//  AboutViewController.m
//  TicToku
//
//  Created by Raza Najam on 5/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"topbar.png"] forBarMetrics:UIBarMetricsDefault];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)sponsorButtonClicked:(id)sender {
    switch ([sender tag]) {
        case 0:{
            NSURL *facebookURL = [NSURL URLWithString: @"fb://profile/940273312689395 "];
            if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
                [[UIApplication sharedApplication] openURL:facebookURL];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/TicToku"]];
            }
            break;
        }
        case 1:{
            NSString *urlString = @"https://twitter.com/TicToku";
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlString]];
            break;
        }
        case 2:{
            NSString *urlString = @"https://www.facebook.com/TicToku";
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlString]];
            break;
        }
        case 3:{
            NSString *urlString = @"http://www.tictoku.com";
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlString]];
            break;
        }
            
        default:
            break;
    }
    
    
}
@end
