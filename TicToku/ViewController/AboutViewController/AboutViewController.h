//
//  AboutViewController.h
//  TicToku
//
//  Created by Raza Najam on 5/17/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

- (IBAction)sponsorButtonClicked:(id)sender;
@end
