//
//  Shared.h
//  AlarmClock
//
//  Created by Raza Najam on 7/17/12.
//  Copyright (c) 2012 Apptellect, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface Shared : NSObject<AVAudioPlayerDelegate>

@property (nonatomic, retain) AVAudioPlayer *myAudioPlayer;

+ (Shared *)instance;
+ (void)showAlert:(NSString *)mesage;
- (void)stopMusic;
- (void)startMusic:(NSString *)musicName;

@end