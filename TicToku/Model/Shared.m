//
//  Shared.m
//  Tictac
//
//  Created by Raza najam
//  Copyright (c) 2015 capbox, Inc. All rights reserved.
//

#import "Shared.h"
@implementation Shared


+ (Shared *)instance    {
    static Shared *object = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        object = [[self alloc] init];
    });
    return object;
}

- (id) init {
    self = [super init];
    if (self != nil) {
        
        
         }
    return self;
}

+(void)showAlert:(NSString *)alertMessage       {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"TicTac" message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(void)startMusic:(NSString *)musicName   {
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:musicName ofType: @"mp3"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
    _myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    [_myAudioPlayer play];
}

-(void)stopMusic    {
    [_myAudioPlayer stop];
}
@end