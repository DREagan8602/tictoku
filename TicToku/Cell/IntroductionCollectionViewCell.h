//
//  IntroductionCollectionViewCell.h
//  TicTac
//
//  Created by Raza Najam on 5/7/15.
//  Copyright (c) 2015 CapBoxStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroductionCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *introductionImageView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextBox;
@property (weak, nonatomic) IBOutlet UILabel *headingTextLabel;

@end
